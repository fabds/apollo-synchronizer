# Apollo Synchronizer

![alt apollo-synchronizer](build/icons/png/64x64.png)


## Installation
````
$ git clone [repo]

$ cd apollo-synchronizer

$ npm install

$ npm start
````

## Make packaging
````
// Mac
$ npm run package-mac

// Windows
$ npm run package-windows

// Linux
$ npm run package-linux
````

## Copyright

2019 - Fabio Della Selva 