/**
 *
 * @type {Electron}
 */
const electron = require('electron');

/**
 *
 * @type {Electron.App}
 */
const app = electron.app;

/**
 *
 * @type {Electron.Tray}
 */
const Tray = electron.Tray;

/**
 *
 * @type {Electron.Menu}
 */
const Menu = electron.Menu;

/**
 *
 * @type {Electron.BrowserWindow}
 */
const BrowserWindow = electron.BrowserWindow;

/**
 * Cron Scheduler
 */
const cron = require('node-cron');

/**
 *
 * @type {Electron.IpcMain}
 */
const ipcMain = electron.ipcMain;

/**
 *
 * @type {module:path}
 */
const path = require('path');

/**
 *
 * @type {module:url}
 */
const url = require('url');

/**
 * Main Window
 */
let mainWindow;

/**
 * Create Main Window
 */
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
      width: 1000,
      height: 800,
      minHeight: 600,
      minWidth: 1000,
      frame: false,
      webPreferences: {
          nodeIntegrationInWorker: true,
          allowEval: false // This is the key!
      }
  });

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'app/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Open the DevTools.
  //mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  });

  /*
  tray = new Tray(path.join(__dirname, 'app/assets/icons/tray.png'));

  const contextMenu = Menu.buildFromTemplate([
      { label: 'Item1', type: 'radio' },
      { label: 'Item2', type: 'radio' },
      { label: 'Item3', type: 'radio', checked: true },
      { label: 'Item4', type: 'radio' }
  ])
  tray.setToolTip('This is my application.')
  tray.setContextMenu(contextMenu)*/

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  //if (process.platform !== 'darwin') {
    app.quit()
  //}
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
