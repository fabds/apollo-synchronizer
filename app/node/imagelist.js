'use strict';

/**
 *
 * @type {request}
 */
const request = require('request');

/**
 *
 * @type {number}
 */
let allImagesCount = 0;

/**
 *
 * @type {number}
 */
let currentImages = 0;

/**
 *
 * @returns {*}
 */
function loadConfiguration(){
    return db.prepare('SELECT * FROM config').get();
};

/**
 * Delete All Images
 */
function deleteAllImages() {
    emptyMessage();
    swal({
        title: "Delete all images?",
        text: "Are you sure that you want to delete all images?",
        icon: "warning",
        buttons: true,
        confirmButtonText: "Delete!",
        dangerMode: true,
    }).then((result) => {
        if (result) {
            db.prepare('DELETE FROM images').run();
            swal("Deleted!", "The images have been deleted succesfully", "success");
            loadImages();
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    });
}

/**
 *
 * @returns {*}
 */
function loadImageElement(){
    return db.prepare('SELECT * FROM images where found_in_folder = 1 and synced = 0 limit 1');
}

/**
 *
 * @returns {*}
 */
function getAllImagesFoundInFolder(){
    let conf = loadConfiguration();
    let sql = 'SELECT * FROM images where found_in_folder = 1';
    if(conf.show_not_synced == 1){
        sql += ' and synced <> 1';
    }
    return db.prepare(sql);
}

/**
 * Toggle sync visibility
 */
function toggleSyncedVisibility(){
    let conf = loadConfiguration();
    let show_not_synced = 1;
    if(conf.show_not_synced == 1){
        show_not_synced = 0;
    }

    var sql = "UPDATE config SET show_not_synced = '"+show_not_synced+"' WHERE id = "+conf.id;
    const stmt = db.prepare(sql);

    let res = stmt.run();
    if(res.changes == 0){
        addMessage('There was an error. Please try again','error');
        loadImages();
    }
    else {
        loadImages();
    }
}

/**
 * toggle synced button visibilty
 */
function toggleSyncedButtonVisibility() {
    let conf = loadConfiguration();
    if(conf.show_not_synced == 1){
        $('#toggleSynced i').addClass('fa-eye').removeClass('fa-eye-slash');
    }
    else {
        $('#toggleSynced i').removeClass('fa-eye').addClass('fa-eye-slash');
    }
}

/**
 * Load All Images
 */
function loadImages(){
    $('#el').empty();
    let conf = loadConfiguration();
    let allImages = getAllImagesFoundInFolder();
    toggleSyncedButtonVisibility();
    let img_count_synced = db.prepare('SELECT count(*) as count FROM images WHERE synced = 1 and found_in_folder = 1').get();
    let img_count = db.prepare('SELECT count(*) as count FROM images WHERE found_in_folder = 1').get();

    $('#total_imgs').html(img_count.count);
    $('#synced_imgs').html(img_count_synced.count);
    $('#visible_imgs').html(allImages.all().length);

    for (const row of allImages.iterate()) {
        let fullPath = slash(path.resolve(conf.product_image_path, row.image_name));
        let fullThumbPath = cacheThumbsPath + thumbPrefix + row.image_name;

        let html = "<tr class='\"row-"+ row.id + "\"'>\n" +
            "<td style='max-width: 100px'><div class='image-container'><a href='load-image.html#" + fullPath + "' target='_blank'><img  data-toggle='tooltip' data-placement='top' title='View Full Image' data-checksum='"+row.checksum+"' src='assets/img/placeholder.jpg' class='lazyload' style='max-width: 200px' data-src='" + fullThumbPath + "' width='100%' onerror=\"javascript:this.src='"+fullPath+"';createThumbnails('"+fullPath+"', $(this))\" /></a></div></td>\n" +
            "<td style='max-width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;'>" + row.image_name + "</td>\n" +
            "<td style='max-width: 50px;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;'>" + row.resolution + "</td>\n" +
            "<td style='max-width: 50px;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;'>" + row.size + " MB</td>\n" +
            "<td class='text-center'><button onclick='toggleStatus(\""+ row.id +"\")' data-toggle='tooltip' data-placement='top' title='"+((row.synced == 2)?"This image has been rejected, press to reset status":"Press to toggle element status")+"' class='btn status-toggle btn-" + ((row.synced == 1) ? "success" : (row.synced == 0)?"danger":"warning") + "'><i class='fa fa-circle text-" + ((row.synced == 1) ? "success" : (row.synced == 0)?"danger":"warning") + "'></i></button></td>\n" +
            "<td class='text-center'><button onclick='createThumbnails(\""+fullPath+"\", $(this).parent().parent().find(\"img\"))' data-toggle='tooltip' data-placement='top' title='Refresh thumb' class='btn btn-primary'><i class='fa fa-image'></i></button></td>\n"+
            "<td style='max-width: 100px' class='text-center'>" + ((row.synced == 1) ? "<button onclick='uploadThisImage(\"" + fullPath + "\")' class='btn btn-warning btn-block'  data-toggle='tooltip' data-placement='top' title='Upload this image again'>Upload</button>" : ((row.found_in_folder == "1") ? "<button onclick='uploadThisImage(\"" + fullPath + "\")' class='btn btn-primary btn-block'  data-toggle='tooltip' data-placement='top' title='Upload this Image'>Upload</button>" : "<button class='btn btn-danger btn-block'>Delete</button>")) + "</td>\n" +
            "</tr>";
        $('#el').append(html);
        lazyload();
        $('[data-toggle="tooltip"]').tooltip();
    }

    if(allImages.all().length==0){
        let html = "<tr>\n" +
            "<td colspan='7' class='text-center'><h4 style='margin:5px; padding:5px'>There are no image to sync</h4></td>\n" + "</tr>";
        $('#el').append(html);
    }
}

/**
 *
 * @param paths
 * @param callback
 */
function uploadImage(paths, callback, all){
    const config = loadConfiguration();
    (typeof(all) !== 'undefined' && all == 'yes')?'':showLoader();

    resize(paths, function(response,error){

        var options = {
            method: 'POST',
            url: config.upload_url,
            headers: {
                'User-Agent': 'client'
            },
            auth: {
                'bearer': config.upload_token
            }
        };


        var req = request.post(options, function (err, resp, body) {
            if (err) {
                let msg = "There was an error uploading image: " + err;
                log(msg,'error','imagelist.js','uploadImage',err);
                addMessage(msg,'error');
                callback(err);
            } else {
                let result = '';
                try {
                    result = JSON.parse(body);

                    let syncStatus = 0;
                    if(resp.statusCode == 201){
                        syncStatus = 1;
                        addMessage(result.Message,'success');
                    }
                    else if(resp.statusCode == 400){
                        syncStatus = 2;
                        addMessage(result.Message,'warning');
                    }
                    else if(resp.statusCode == 301){
                        syncStatus = 0;
                        addMessage(result.Message,'error');
                        callback(result.Message);
                    }

                    let sqlUpdate = "UPDATE images SET synced = '"+syncStatus+"' WHERE image_name = '"+path.basename(paths)+"' ";
                    let stmt = db.prepare(sqlUpdate);
                    let upd = stmt.run();

                    if(upd.changes == 0){
                        log('There was an error updating images in DB','error','imagelist.js','uploadImage',upd)
                    }
                    else {
                        loadImages();
                        callback(err,result)
                    }
                    log('Server response for image upload ' + body,'debug','imagelist.js','uploadImage');

                } catch (e) {
                    log(e,'error','imagelist.js','uploadImage');
                    addMessage('There was an error please check url and token','error');
                    callback('There was an error');
                }

                //else if response rejected warning
            }
            hideLoader();
        });
        var form = req.form();
        form.append('file', fs.createReadStream(cacheResizedPath + path.basename(paths)), {
            filename: path.basename(paths).toLowerCase(),
            contentType: 'image/jpeg'
        });

        log(req,'debug','imagelist.js','uploadImage');
    })
}

/**
 *
 * @param el
 */
function uploadThisImage(el){
    uploadImage(el, function(err,response){
        log(response);
    });
}

/**
 *
 * @param id
 */
function toggleStatus(id){
    let el = db.prepare('SELECT synced FROM images WHERE id = '+id).get();
    let new_status = (el.synced == 0)?1:0;

    let sqlUpdate = "UPDATE images SET synced = '"+new_status+"' WHERE id = '" + id + "' ";
    let stmt = db.prepare(sqlUpdate);
    let upd = stmt.run();

    if(upd.changes == 0){
        log(upd)
    }
    else {
        loadImages();
    }
}

/**
 *
 * @param status
 */
function setAllSyncedToStatus(status) {
    emptyMessage();
    let newStatusLabel = (status)?"'SYNCED'":"'NOT SYNCED'";
    swal({
        title: "Change all images?",
        text: "Change all images to "+newStatusLabel+" status",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            let sqlUpdate = "UPDATE images SET synced = '"+status+"' ";
            let stmt = db.prepare(sqlUpdate);
            let upd = stmt.run();

            if(upd.changes == 0){
                log(upd)
            }
            else {
                loadImages();
            }
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    });
}

/**
 * Stop All Upload
 */
function stopUploadAll() {
    swal({
        title: "Are you sure to stop the process?",
        text: "Stop upload",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            navigateToUrl('image-list.html');
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    })
}

/**
 * Init Upload
 */
function initUploadAll() {
    allImagesCount = 0;
    currentImages = 0;
    stop = false;
    let al = db.prepare('SELECT count(*) as c FROM images where found_in_folder = 1 and synced = 0').get();
    allImagesCount = al.c;
    $('.total_loading').html(allImagesCount);
    $('.current_loading').html(currentImages);
    $('.loader-all').fadeIn('slow');
    uploadAll();
}

/**
 * Upload All Images
 */
function uploadAll() {
    const conf = loadConfiguration();
    let allImages = loadImageElement();

    if(currentImages == allImagesCount){
        $('.loader-all').fadeOut('slow');
    }

    for (const row of allImages.iterate()) {
        uploadImage(conf.product_image_path + "/" + row.image_name, function(err,response){
            if(!err){
                currentImages++;
                $('.current_loading').html(currentImages);
                if(currentImages != allImagesCount){
                    uploadAll();
                }
                else {
                    $('.loader-all').fadeOut('slow');
                }
            }
            else {
                $('.loader-all').fadeOut('slow');
            }
        }, 'yes');
    }

    loadImages();
}

/**
 * Confirm upload all
 */
function confirmUploadAll() {
    emptyMessage();
    swal({
        title: "Upload all images?",
        text: "The operation could be long",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            initUploadAll();
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    })
}

/**
 * Init
 */
$(document).ready(function () {
    loadImages();
});