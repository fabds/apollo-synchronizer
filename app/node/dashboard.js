'use strict'

if (db) {
    const img_count = db.prepare('SELECT count(*) as count FROM images').get();
    const img_count_synced = db.prepare('SELECT count(*) as count FROM images WHERE synced = 1').get();
    const img_count_rejected = db.prepare('SELECT count(*) as count FROM images WHERE synced = 2').get();
    const img_count_not_found = db.prepare('SELECT count(*) as count FROM images WHERE found_in_folder = 0').get();

    $('#imageCount').html(img_count.count);
    $('#imageCountSynced').html(img_count_synced.count);
    $('#imageCountRejected').html(img_count_rejected.count);
    $('#imageCountNotFound').html(img_count_not_found.count);

    db.close();
}