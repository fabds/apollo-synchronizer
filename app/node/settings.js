'use strict';

/**
 * Dialog
 */
const {dialog} = require('electron').remote;

/**
 * Select Directory
 */
document.querySelector('#selectBtn').addEventListener('click', function (event) {
    dialog.showOpenDialog({
        properties: ['openDirectory']
    }, function (files) {
        if (files !== undefined) {
            $('#image_path').val(files);
        }
    });
});

/**
 * Load Configuration
 */
function loadConfiguration(){
    let config = db.prepare('SELECT * FROM config').get();
    $('#config_id').val(config.id);
    $('#image_path').val(config.product_image_path);
    $('#upload_token').val(config.upload_token);
    $('#upload_url').val(config.upload_url);
    $('#color').val(config.color);
}

/**
 * Save Settings
 */
$('#save-settings').on('click', function(){
    var sql = "UPDATE config SET product_image_path = '"+$('#image_path').val()+"' , upload_token = '"+$('#upload_token').val()+"', upload_url = '"+$('#upload_url').val()+"', color = '"+$('#color').val()+"' WHERE id = "+$('#config_id').val();
    const stmt = db.prepare(sql);

    let res = stmt.run()
    if(res.changes == 0){
        $('#message').html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;&nbsp;&nbsp;&nbsp;</a> <i class="fa fa-warning"></i> '+err+'</div>');
    }
    else {
        $('#message').html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;&nbsp;&nbsp;&nbsp;</a> <i class="fa fa-check"></i> Configuration Saved</div>');
        syncFolder();
        initUi();
    }
});

/**
 * Get Cache Info
 */
function getCacheInfo() {
    fs.readdir(cacheThumbsPath, (err, files) => {
        if(err){
            addMessage('There was an error reading cache folder','warning','#message-maintenance');
        }
        else {
            if (typeof(files) === 'undefined') {
                addMessage('There are no image in this folder','warning','#message-maintenance');
            }
            files = files.filter(checkImageExtension);
            var numFiles = files.length;
            $('.numThumb').html(numFiles);
        }
    });
    fs.readdir(cacheResizedPath, (err, files) => {
        if(err){
            addMessage('There was an error reading cache folder','warning','#message-maintenance');
        }
        else {
            if (typeof(files) === 'undefined') {
                addMessage('There are no image in this folder','warning','#message-maintenance');
            }
            files = files.filter(checkImageExtension);
            var numFiles = files.length;
            $('.numResized').html(numFiles);
        }
    });
}

/**
 * Flush Cache
 */
function flushImageCache(){
    fs.readdir(cacheThumbsPath, (err, files) => {
        if(err){
            addMessage('There was an error reading cache folder','warning','#message-maintenance');
        }
        else {
            if (typeof(files) === 'undefined') {
                addMessage('There are no image in this folder','warning','#message-maintenance');
            }
            files = files.filter(checkImageExtension);
            files.forEach(file => {
                let fullPath = cacheThumbsPath + "/" + file;
                fs.unlinkSync(fullPath);
            });
            getCacheInfo();
            addMessage('The cache files have been deleted','success','#message-maintenance');
        }
    });
    fs.readdir(cacheResizedPath, (err, files) => {
        if(err){
            addMessage('There was an error reading cache folder','warning','#message-maintenance');
        }
        else {
            if (typeof(files) === 'undefined') {
                addMessage('There are no image in this folder','warning','#message-maintenance');
            }
            files = files.filter(checkImageExtension);
            files.forEach(file => {
                let fullPath = cacheResizedPath + "/" + file;
                fs.unlinkSync(fullPath);
            });
            getCacheInfo();
            addMessage('The cache files have been deleted','success','#message-maintenance');
        }
    });
}

/**
 * Flush Cache Confirm
 */
function flushImageCacheConfirm() {
    emptyMessage();

    swal({
        title: "Flush all image cache?",
        text: "Are you sure? The operation is not reversible",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            flushImageCache();
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    });
}

/**
 * Reset App
 */
function resetApp() {
    const config = db.prepare('SELECT id FROM config').get();
    var sql = "UPDATE config SET product_image_path = '' , upload_token = '', upload_url = '', color = 'blue' WHERE id = "+config.id;
    const stmt = db.prepare(sql);
    let res = stmt.run()
    if(res.changes == 0){
        addMessage("There was an error",'error','#message-maintenance');
    }
    else {
        initUi();
        loadConfiguration();
        db.prepare('DELETE FROM images').run();
        flushImageCache();
        swal('Done', 'The app has been resetted','success');
        emptyMessage();
    }
}

/**
 * Reset App Confirm
 */
function resetAppConfirm() {
    emptyMessage();

    swal({
        title: "Do you want to reset app?",
        text: "The operation is not reversible",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            resetApp();
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    });
}

/**
 * Upload DB
 */
document.querySelector('#uploadDB').addEventListener('click', function (event) {
    dialog.showOpenDialog({
        properties: ['openFile'],
        filters: [
            { name: "db*", extensions: ["sqlite3"] }
        ]
    }, function (files) {
        if(typeof(files) !== 'undefined'){
            fs.copyFileSync(files[0], DbBackupPath + '/db_' + currentDate() + '.sqlite3');
            getBackupList();
            swal({
                title: "Done",
                text: "The Db has been imported",
                icon: "success",
                buttons: {
                    cancel: false,
                    confirm: true,
                },
                dangerMode: false
            }).then((result) => {
            });
        }
    });
});

/**
 * Get Backup List
 */
function getBackupList() {
    $('#dbBkList').html("<option value=''>Select DB Backup</option>");
    fs.readdir(DbBackupPath, (err, files) => {
        if(err){
            addMessage('There was an error reading DB Backup folder','warning','#message-maintenance');
        }
        else {
            files = files.filter(checkDbExtension);
            files.forEach(file => {
                let fullPath = DbBackupPath + "/" + file;
                $('#dbBkList').append("<option value='"+fullPath+"'>"+file+"</option>");
            });
        }
    });
}

/**
 * Backup DB
 */
function backupDB(){
    let rootPath = path.resolve(__dirname, '../') + '/';
    fs.copyFileSync(rootPath + 'db.sqlite3', DbBackupPath + '/db_' + currentDate() + '.sqlite3');
    getBackupList();
    swal({
        title: "Done",
        text: "The Db backup has been done",
        icon: "success",
        buttons: {
            cancel: false,
            confirm: true,
        },
        dangerMode: false
    }).then((result) => {
    });
}

/**
 * Backup DB Confirm
 */
function backupDBConfirm() {
    emptyMessage();

    swal({
        title: "Do you want to Backup this configuration?",
        text: "Backup Configuration?",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            backupDB();
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    });
}

/**
 * Delete All Db Backup
 */
function deleteAllDbBackup() {
    fs.readdir(DbBackupPath, (err, files) => {
        if(err){
            addMessage('There was an error reading DB Backup folder','warning','#message-maintenance');
        }
        else {
            if (typeof(files) === 'undefined') {
                addMessage('There are no backup in this folder','warning','#message-maintenance');
            }
            files = files.filter(checkDbExtension);
            files.forEach(file => {
                let fullPath = DbBackupPath + "/" + file;
                fs.unlinkSync(fullPath);
            });

            getBackupList();
            swal({
                title: "Done",
                text: "The Db backup have been deleted succesfully",
                icon: "success",
                buttons: {
                    cancel: false,
                    confirm: true,
                },
                dangerMode: false
            }).then((result) => {
            });
        }
    });
}

/**
 * Delete all Db Backup Confirm
 */
function deleteAllDbBackupConfirm() {
    emptyMessage();

    swal({
        title: "Do you want to delete all backup?",
        text: "The operation is not reversible",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            deleteAllDbBackup();
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    });
}

/**
 * Delete This Backup
 */
function deleteThisBackup(){
    let file = $('#dbBkList').val();
    if(file!=''){
        fs.unlinkSync(file);
        getBackupList();
        swal({
            title: "Done",
            text: "The Db backup has been deleted succesfully",
            icon: "success",
            buttons: {
                cancel: false,
                confirm: true,
            },
            dangerMode: false
        }).then((result) => {
        });
    }
}

/**
 * Delete all Db Backup Confirm
 */
function deleteThisBackupConfirm() {
    emptyMessage();
    let file = $('#dbBkList').val();
    if(file==''){
        alertSelectFile();
        return;
    }

    swal({
        title: "Do you want to delete this backup?",
        text: "The operation is not reversible",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            deleteThisBackup();
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    });
}

/**
 * Import This Backup
 */
function importThisBackup(){
    let rootPath = path.resolve(__dirname, '../') + '/';
    let currentFile = rootPath + 'db.sqlite3';
    let file = $('#dbBkList').val();
    if(file!=''){
        fs.unlinkSync(currentFile);
        fs.copyFileSync(file, currentFile);
        swal({
            title: "Done",
            text: "The Db backup has been imported succesfully",
            icon: "success",
            buttons: {
                cancel: false,
                confirm: true,
            },
            dangerMode: false
        }).then((result) => {
            location.reload();
        });
    }
}

/**
 * Import this Db Backup Confirm
 */
function importThisBackupConfirm() {
    emptyMessage();
    let file = $('#dbBkList').val();
    if(file==''){
        alertSelectFile();
        return;
    }

    swal({
        title: "Do you want to import this backup?",
        text: "The operation is not reversible",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((result) => {
        if (result) {
            importThisBackup();
        } else {
            swal("Canceled!", "The operation was canceled", "warning");
        }
    });
}

function exportThisBackup(){
    let file = $('#dbBkList').val();
    if(file!=''){
        location.href = file;
    }
    else {
        alertSelectFile();
    }
}

/**
 * Alert Select File
 */
function alertSelectFile() {
    swal("Warning!", "Please select a backup file first", "warning");
}

/**
 *  Get Current Date
 *
 * @returns {string}
 */
function currentDate() {
    let currentdate = new Date();
    let datetime = currentdate.getFullYear() + ""
        + (currentdate.getMonth()+1)  + ""
        + currentdate.getDate() + ""
        + currentdate.getHours() + ""
        + currentdate.getMinutes() + ""
        + currentdate.getSeconds();

    return datetime;
}

/**
 * Init Page
 */
$(document).ready(function () {
    loadConfiguration();
    getCacheInfo();
    getBackupList();
});