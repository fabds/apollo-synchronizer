/**
 *
 * @type {module:path}
 */
const path = require('path');

/**
 *
 * @type {module:url}
 */
const url = require('url');

/**
 *
 * @type {string}
 */
const dbPath = path.resolve(__dirname, '../../db.sqlite3').replace('/app.asar','');

/**
 *
 * @type {*|Database}
 */
const Database = require('better-sqlite3');

/**
 *
 * @type {Database}
 */
const db = new Database(dbPath, {  });

/**
 *
 * @type {thumb}
 */
const thumb = require('node-thumbnail').thumb;

/**
 *
 * @type {string[]}
 */
const acceptedExtensions = ['jpg','jpeg','png','gif'];

/**
 *
 * @type {string}
 */
const cacheImagePath = path.resolve(__dirname, '../../app/cache/images/');

/**
 *
 * @type {string}
 */
const thumbPrefix = 'thumb_';

/**
 *
 * @type {number}
 */
const thumbSize = 300;

/**
 *
 * @type {string}
 */
const cacheThumbsPath = cacheImagePath + '/thumbs/';

/**
 *
 * @type {string}
 */
const cacheResizedPath = cacheImagePath + '/resized/';

/**
 *
 * @param imageFolder
 */
function createThumbnails(imageFolder){
    thumb({
        source: imageFolder,
        destination: cacheThumbsPath,
        concurrency: 1,
        width: thumbSize,
        overwrite: true,
        prefix: thumbPrefix,
        suffix: "",
        ignore: true,
        skip: true,
        quiet: false
    }, function(files, err, stdout, stderr) {
        console.log('done');
    })
}

function process() {
    console.log('Creating thumb');
    const config = db.prepare('SELECT product_image_path FROM config').get();
    createThumbnails(config.product_image_path);
    console.log('End creating thumb');
}

process.on("message", (data) => {
    console.log('sdasd');
    process();
    callback(null,rawBody);
});