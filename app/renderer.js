'use strict'

window.$ = window.jQuery = require('jquery')
window.Tether = require('tether')
window.Bootstrap = require('bootstrap')

/**
 *
 * @type {boolean}
 */
const consoleLogEnable = true;

/**
 *
 * @type {boolean}
 */
const loggerEnable = true;

/**
 *
 * @type {module:path}
 */
const path = require('path');

/**
 *
 * @type {(path: string) => string}
 */
const slash = require('slash');

/**
 * Sweet Alert
 */
const swal = require('sweetalert');

/**
 *
 * @type {string}
 */
const LogPath = path.resolve(__dirname, '../storage/logs/');

/**
 *
 * @type {{timeZone: string, folderPath: string, dateBasedFileNaming: boolean, fileNameSuffix: string, fileNameExtension: string, dateFormat: string, timeFormat: string, logLevel: string, onlyFileLogging: boolean}}
 */
/*
const options = {
    timeZone: 'Europe/Rome',
    folderPath: LogPath,
    dateBasedFileNaming: true,
    fileNameSuffix: '_synchronizer',
    fileNameExtension: '.log',
    dateFormat: 'YYYY-MM-DD',
    timeFormat: 'HH:mm:ss.SSS',
    logLevel: 'debug',
    onlyFileLogging: true
};
*/
/**
 * Logger
 */
//let logger = require('node-file-logger');

/**
 * Init Logger
 */
//logger.SetUserOptions(options);

/**
 *
 * @type {string}
 */
const dbPath = path.resolve(__dirname, '../db.sqlite3').replace('/app.asar','');

/**
 *
 * @type {*|Database}
 */
const Database = require('better-sqlite3');

/**
 *
 * @type {Database}
 */
const db = new Database(dbPath, {  });

/**
 *
 * @type {module:fs}
 */
const fs = require('fs');

/**
 *
 */
const sizeOf = require('image-size');

/**
 *
 * @type {string}
 */
const DbBackupPath = path.resolve(__dirname, '../storage/backup/db/');

/**
 *
 * @type {module:crypto}
 */
const crypto = require('crypto');

/**
 *
 * @type {thumb}
 */
const thumb = require('node-thumbnail').thumb;

/**
 *
 * @type {string[]}
 */
const acceptedExtensions = ['jpg','jpeg','png','gif'];

/**
 *
 * @type {string[]}
 */
const acceptedDbExtensions = ['sqlite3'];

/**
 *
 * @type {string}
 */
const cacheImagePath = path.resolve(__dirname, '../storage/cache/images/');

/**
 *
 * @type {string}
 */
const thumbPrefix = 'thumb_';

/**
 *
 * @type {number}
 */
const thumbSize = 300;

/**
 *
 * @type {string}
 */
const cacheThumbsPath = cacheImagePath + '/thumbs/';

/**
 *
 * @type {string}
 */
const cacheResizedPath = cacheImagePath + '/resized/';

/**
 *
 * @type {md5File}
 */
const md5File = require('md5-file');

/**
 *
 * @type {Array}
 */
let generatingThumbs = [];

/**
 * Show Loader
 */
function showLoader(){
    $('.loader').show();
}

/**
 * Hide Loader
 */
function hideLoader(){
    $('.loader').fadeOut('slow')
}

/**
 * Check image extension
 *
 * @param filename
 * @returns {boolean}
 */
function checkImageExtension(filename) {
    let response = true;
    try {
        if(typeof(filename)!=="undefined" && typeof(filename.split('.')[1])!=='undefined'){
            if(acceptedExtensions.indexOf(filename.split('.')[1].toLowerCase()) === -1){
                response = false;
            }
        }
    }
    catch(err) {
        alert(err);
        response = false;
    }

    return response;
}

function checkDbExtension(filename) {
    let response = true;
    try {
        if(typeof(filename)!=="undefined" && typeof(filename.split('.')[1])!=='undefined'){
            if(acceptedDbExtensions.indexOf(filename.split('.')[1].toLowerCase()) === -1){
                response = false;
            }
        }
    }
    catch(err) {
        alert(err);
        response = false;
    }

    return response;
}

/**
 *
 * @param str
 * @param algorithm
 * @param encoding
 * @returns {string}
 */
function generateChecksum(str, algorithm, encoding) {
    return crypto
        .createHash(algorithm || 'md5')
        .update(str, 'utf8')
        .digest(encoding || 'hex');
}

/**
 * syncFolder
 */
function syncFolder(){
    showLoader();
    const config = db.prepare('SELECT product_image_path FROM config').get();

    fileFromFolder(config.product_image_path, function (err, content) {
        if(!err){
            hideLoader();
            loadImages();
            addMessage("The folder was synced",'success');
            //createThumbnails(row.product_image_path);
        }
        else {
            hideLoader();
            addMessage("There was an error. Can't read the folder",'error');
        }
    });

}

/**
 * initUi
 */
function initUi() {
    const config = db.prepare('SELECT color FROM config').get();
    $('.sidebar-properties').attr('data-color',config.color);
}

/**
 *
 * @param imageFolder
 * @param el
 */
function createThumbnails(imageFolder,el){
    var checksum = el.data('checksum');
    var element = el.parent().parent();
    element.prepend('<span class="generating-thumbnails '+checksum+'"><i class="fa fa-spinner fa-spin"></i><br/>Generating Thumb</span>');
    generatingThumbs.push(checksum);
    thumb({
        source: imageFolder,
        destination: cacheThumbsPath,
        concurrency: 1,
        width: thumbSize,
        overwrite: true,
        prefix: thumbPrefix,
        suffix: "",
        ignore: true,
        skip: true,
        quiet: true
    }, function(files, err, stdout, stderr) {
        removeThumbTxt();
    })
}

/**
 * removeThumbTxt
 */
function removeThumbTxt(){
    log(generatingThumbs)
    $('.'+generatingThumbs[0]).remove();
    generatingThumbs.shift();
}

/**
 *
 * @param imageFolder
 * @param callback
 */
function resize(imageFolder,callback){
    thumb({
        source: imageFolder,
        destination: cacheResizedPath,
        concurrency: 1,
        width: 1280,
        overwrite: true,
        prefix: '',
        suffix: "",
        ignore: false,
        skip: true,
        quiet: true
    }, function(files, err, stdout, stderr) {
        callback(files,err)
    });
}

/**
 *
 * @param imageFolder
 * @returns {Array}
 */
var fileFromFolder = function getFilesFromFolder(imageFolder, callback){
    let result;
    fs.readdir(imageFolder, (err, files) => {
        if(err){
            return callback(err);
        }
        else {
            if (typeof(files) === 'undefined') {
                let html = "<tr><td colspan='6'>There are no image in this folder</td></tr>";
                $('#el').append(html);
                return callback(err);
            }
            files = files.filter(checkImageExtension);
            var numFiles = files.length;
            $('.num_file').html(numFiles);

            let stmt = db.prepare('UPDATE images SET found_in_folder = 0');
            let update = stmt.run();
            if(update.changes == 0) {
                log(update);
            }

            files.forEach(file => {
                let fullPath = imageFolder + "/" + file;
                let fullThumbPath = cacheThumbsPath + thumbPrefix + file;

                let checksum = generateChecksum(fullPath);
                let stats = fs.statSync(fullPath);
                if (stats.isFile()) {
                    let size = getFilesizeInMegaBytes(fullPath);
                    let dimensions = sizeOf(fullPath);

                    stmt = db.prepare('SELECT * FROM images WHERE image_name = ?');
                    let row = stmt.get(file);

                    if (row) {
                        let sqlUpdate = "UPDATE images SET found_in_folder = '1' "
                        if (checksum !== row.checksum) {
                            sqlUpdate += ", synced = 0 "
                        }

                        sqlUpdate += "WHERE image_name = '" + file + "'";
                        stmt = db.prepare(sqlUpdate);

                        let upd = stmt.run();
                        if(upd.changes == 0){
                            log(upd)
                        }
                    }
                    else {
                        let sqlInsert = "INSERT INTO images (image_name, synced, checksum, found_in_folder, resolution, size) VALUES ('" + file + "', '0', '" + checksum + "','1','" + dimensions.width + "x" + dimensions.height + "','" + size + "');";
                        stmt = db.prepare(sqlInsert);

                        let ins = stmt.run();
                        if(ins.changes == 0){
                            log(ins)
                        }
                    }

                }
            });
            return callback(null,'ok');
        }
    });
};

/**
 *
 * @param filename
 * @returns {number}
 */
function getFilesizeInMegaBytes(filename) {
    const stats = fs.statSync(filename);
    const fileSizeInBytes = stats.size / 1000000.0;
    return fileSizeInBytes;
}

/**
 * Empty Message
 */
function emptyMessage(){
    $('#message').empty();
    $('#message-maintenance').empty();
    $('#message-updater').empty();
}

/**
 *
 * @param msg
 * @param type
 */
function appendMessage(msg,type,container){
    if(typeof (container) === 'undefined' || container == '' || container == null){
        container = '#message';
    }
    if(type == "success"){
        $(container).html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;&nbsp;&nbsp;&nbsp;</a> <i class="fa fa-check"></i> '+msg+'</div>');
    }
    else if(type == "error"){
        $(container).html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;&nbsp;&nbsp;&nbsp;</a> <i class="fa fa-warning"></i> '+msg+'</div>');
    }
    else if(type == "warning"){
        $(container).html('<div class="alert alert-warning alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;&nbsp;&nbsp;&nbsp;</a> <i class="fa fa-warning"></i> '+msg+'</div>');
    }
}

/**
 *
 * @param msg
 * @param type
 */
function addMessage(msg,type,container){
    emptyMessage();
    appendMessage(msg,type,container);
}

/**
 *
 * @param url
 */
function navigateToUrl(url){
    $("body").fadeOut('fast');
    setTimeout(function(){location.href=url},200);
}

/**
 *
 */
function initPage() {
    $('body').hide();
    $("body").fadeIn('slow');
}

/**
 *
 * @param obj
 * @param type
 */
function log(msg,type,service,method,obj){
    if(consoleLogEnable){
        console.log(msg);
    }

    /*
    if(loggerEnable){
        if(typeof(type) === 'undefined' || type == null || type == '' || type == 'debug'){
            logger.Debug(JSON.stringify(msg),service,method,obj);
        }
        else if (type == 'error'){
            logger.Debug(JSON.stringify(msg),service,method,obj);
        }
        else if (type == 'fatal'){
            logger.Fatal(JSON.stringify(msg),service,method,obj);
        }
        else if (type == 'warning'){
            logger.Warn(JSON.stringify(msg),service,method,obj);
        }
        else if (type == 'info'){
            logger.Info(JSON.stringify(msg),service,method,obj);
        }
    }*/
}

initPage();
showLoader();
initUi();
$(document).ready(function() {
    hideLoader();
    $('[data-toggle="tooltip"]').tooltip();
});

var { remote } = require('electron');

function init() {
    document.getElementById("min-btn").addEventListener("click", function (e) {
        var window = remote.getCurrentWindow();
        window.minimize();
    });

    document.getElementById("max-btn").addEventListener("click", function (e) {
        var window = remote.getCurrentWindow();
        window.maximize();
    });

    document.getElementById("close-btn").addEventListener("click", function (e) {
        var window = remote.getCurrentWindow();
        window.close();
    });
};

document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        init();
    }
};

